/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: porttimer.c,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

#include "appfunctions.h"
/* ----------------------- static functions ---------------------------------*/
static void prvvTIMERExpiredISR( void );

volatile BOOL response_timeout = false;

/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBPortTimersInit( USHORT usTim1Timerout50us )
{
    /* Structure of initialize PIT */
    pit_config_t pitConfig;

    /*
     * pitConfig.enableRunInDebug = false;
     */
    PIT_GetDefaultConfig(&pitConfig);

    /* Init pit module */
    PIT_Init(PIT, &pitConfig);


    /* Set timer period for channel 0 */
    PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(usTim1Timerout50us*50, PIT_SOURCE_CLOCK));

    PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag);

    /* Enable timer interrupts for channel 0 */
    PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);


    /* Set timer period for channel 1 */
    PIT_SetTimerPeriod(PIT, TIMER_RESPONSE_CHANNEL, MSEC_TO_COUNT(RESPONSE_TIMEOUT_MS, PIT_SOURCE_CLOCK));

    /* Enable timer interrupts for channel 1 */
    PIT_EnableInterrupts(PIT, TIMER_RESPONSE_CHANNEL, kPIT_TimerInterruptEnable);

    /* Enable at the NVIC */
    EnableIRQ(PIT_IRQ_35_ID);
    EnableIRQ(PIT_IRQ_RESPONSE_ID);
    return TRUE;
}



inline void vMBPortTimersEnable(pit_chnl_t channel) {
	/* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
	/* Start channel 0 */
	//PRINTF("\r\nStarting channel No.0 ...");
	if (channel == TIMER_RESPONSE_CHANNEL) {
		response_timeout = false;
	}
	PIT_ClearStatusFlags(PIT, channel, kPIT_TimerFlag);
	PIT_StopTimer(PIT, channel);
	PIT_StartTimer(PIT, channel);
}

inline void vMBPortTimersDisable(pit_chnl_t channel) {
	/* Disable any pending timers. */
	PIT_StopTimer(PIT, channel);
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
static void prvvTIMERExpiredISR(void) {
	/* Clear interrupt flag.*/
	(void) pxMBPortCBTimerExpired();
	//LED_TOGGLE();
	PIT_ClearStatusFlags(PIT, TIMER_35_CHANNEL, kPIT_TimerFlag);
}

void PIT_IRQ_35_HANDLER() {
	prvvTIMERExpiredISR();
}

void PIT_IRQ_RESPONSE_HANDLER() {
	response_timeout = true;
	PIT_ClearStatusFlags(PIT, TIMER_RESPONSE_CHANNEL, kPIT_TimerFlag);
}
