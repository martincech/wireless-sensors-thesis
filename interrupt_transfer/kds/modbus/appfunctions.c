/*
 * appfunctions.c
 *
 *  Created on: Nov 2, 2016
 *      Author: David Danaj
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbframe.h"
#include "mbproto.h"
#include "mbconfig.h"
#include "mbrtu.h"

#include "appfunctions.h"

BOOL processed = false;
extern volatile BOOL response_timeout;
volatile SHORT registers_array[MAX_REGS_READ];
UCHAR currPacket;
unsigned char txFrame[BUFFER_SIZE];
USHORT frameSize;
Packet packets[NUM_OF_PREDEFINED_PACKETS];
Packet packet;


eMBException processHoldingRegisters(UCHAR *rxFrame, USHORT *usLen) {
	eMBException status = MB_EX_NONE;

	if (rxFrame[0] == packet.function) {

		// frame[1] - Byte count returned from slave
		// packet->data - quantity of registers requested
		// *2 - register is 16bit -> 2 bytes
		if (rxFrame[1] == (packet.data * 2)) {
			UCHAR index = 2;
			for (unsigned int i = 0; (i < packet.data) && (i < MAX_REGS_READ); i++) {
				registers_array[i] = (rxFrame[index] << 8) | rxFrame[index + 1];
				index +=2;
			}
			processed = true;
		} else {
			status = MB_EX_ILLEGAL_DATA_VALUE;
		}
	} else {
		status = MB_EX_ILLEGAL_FUNCTION;
	}
	return status;
}

SHORT *readHoldingRegisters(UCHAR slave_id, USHORT reg_addr, USHORT number_of_regs) {
	processed = false;
	modbus_construct(&packet, slave_id, MB_FUNC_READ_HOLDING_REGISTER, reg_addr, number_of_regs);
	createPacket(&packet);
	eMBRTUSend(packet.slave_id, txFrame, frameSize);
	vMBPortTimersEnable(TIMER_RESPONSE_CHANNEL);

	while (!processed) {
		if (response_timeout) {
			vMBPortTimersDisable(TIMER_RESPONSE_CHANNEL);
			return NULL;
		}
		if (eMBPoll(packet.slave_id) != MB_ENOERR) {
			return NULL;
		}
	}
	return &registers_array[0];
}



void createPacket(Packet *_packet) {
	txFrame[0] = _packet->function;
	txFrame[1] = _packet->address >> 8; // address Hi
	txFrame[2] = _packet->address & 0xFF; // address Lo
	txFrame[3] = _packet->data >> 8; // MSB
	txFrame[4] = _packet->data & 0xFF; // LSB
	frameSize = 5;
}

void modbus_construct(Packet *_packet, unsigned char slave_id, unsigned char function,
	unsigned int address, unsigned int data) {
	_packet->slave_id = slave_id;
	_packet->function = function;
	_packet->address = address;
	_packet->data = data;
}

