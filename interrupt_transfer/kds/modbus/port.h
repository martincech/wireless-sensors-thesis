/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: port.h,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */

#ifndef _PORT_H
#define _PORT_H

#include <assert.h>
#include <inttypes.h>
#include "fsl_pit.h"
#include "fsl_uart.h"
#include "board.h"
//#include "fsl_common.h"
//#include "fsl_debug_console.h"


#define UART_CLKSRC kCLOCK_CoreSysClk
/* Get source clock for PIT driver */
#define PIT_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_BusClk)
#define TIMER_35_CHANNEL kPIT_Chnl_0
#define PIT_IRQ_35_HANDLER PIT0_IRQHandler
#define PIT_IRQ_35_ID PIT0_IRQn

#define TIMER_RESPONSE_CHANNEL kPIT_Chnl_1
#define PIT_IRQ_RESPONSE_HANDLER PIT1_IRQHandler
#define PIT_IRQ_RESPONSE_ID PIT1_IRQn

//#define UART_IRQ_HANDLER UART0_RX_TX_IRQHandler
#define UART_IRQ_HANDLER UART1_RX_TX_IRQHandler

#define LED_TOGGLE() LED_RED_TOGGLE()

#define	INLINE                      inline
#define PR_BEGIN_EXTERN_C           extern "C" {
#define	PR_END_EXTERN_C             }

#define ENTER_CRITICAL_SECTION( ) enterCriticalSection()
#define EXIT_CRITICAL_SECTION( ) exitCriticalSection()

typedef uint8_t BOOL;

typedef unsigned char UCHAR;
typedef char CHAR;

typedef uint16_t USHORT;
typedef int16_t SHORT;

typedef uint32_t ULONG;
typedef int32_t LONG;

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

void enterCriticalSection(void);
void exitCriticalSection(void);
#endif
