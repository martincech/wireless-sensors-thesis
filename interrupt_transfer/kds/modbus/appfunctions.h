/*
 * appfunctions.h
 *
 *  Created on: Nov 2, 2016
 *      Author: David Danaj
 */

#ifndef MODBUS_APPFUNCTIONS_H_
#define MODBUS_APPFUNCTIONS_H_

#include "mbproto.h"

#define MAX_REGS_READ 20
#define BUFFER_SIZE 64

#define NUM_OF_PREDEFINED_PACKETS 1
#define TEMP_PACKET 0

#define RESPONSE_TIMEOUT_MS 1000


typedef struct {
	// specific packet info
	unsigned char slave_id;
	unsigned char function;
	unsigned int address;

	// For functions 1 & 2 data is the number of points
	// For function 5 data is either ON (oxFF00) or OFF (0x0000)
	// For function 6 data is exactly that, one register's data
	// For functions 3, 4 & 16 data is the number of registers
	// For function 15 data is the number of coils
	unsigned int data;
} Packet;



SHORT *readHoldingRegisters(UCHAR slave_id, USHORT reg_addr, USHORT number_of_regs);
eMBException processHoldingRegisters(UCHAR *rxFrame, USHORT *usLen);
void createPacket(Packet *_packet);
void modbus_construct(Packet *_packet, unsigned char slave_id, unsigned char function,
	unsigned int address, unsigned int data);

#endif /* MODBUS_APPFUNCTIONS_H_ */
