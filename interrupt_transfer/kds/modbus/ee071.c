/*
 * ee071.c
 *
 *  Created on: 6 Nov 2016
 *      Author: D�vid Danaj
 */

#include "ee071.h"
#include "appfunctions.h"

SHORT readTemperature() {
	return readRegister(EE071_TEMPERATURE_ADDR);
}

SHORT readRelHumidity() {
	return readRegister(EE071_REL_HUMIDITY_ADDR);
}

BOOL readTempAndHumidity(SHORT *temp, SHORT *hum) {
	SHORT *result;
	/* Registers in EE071:
	 30040 0x27 Temperature [�C]
	 30041 0x28 Temperature [�F]
	 30042 0x29 Rel Humidity [%]
	*/
	result = readHoldingRegisters(EE071_SLAVE_ID, EE071_TEMPERATURE_ADDR, 3);
	if (result == NULL) {
		*temp = INT16_MAX;
		*hum = INT16_MAX;
		return FALSE;
	}
	*temp = *result;
	*hum = *(result + 2);
	return TRUE;
}

SHORT readRegister(USHORT addr) {
	SHORT *result;
	result = readHoldingRegisters(EE071_SLAVE_ID, addr, 1);
	if (result == NULL) {
		return INT16_MAX;
	}
	return *result;
}


