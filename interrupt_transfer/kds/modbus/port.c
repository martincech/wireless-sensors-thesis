/*
 * port.c
 *
 *  Created on: Oct 31, 2016
 *      Author: David Danaj
 */

#include "port.h"
#include <inttypes.h>
#include "fsl_common.h"
uint32_t primask;

void enterCriticalSection(void) {
	primask = DisableGlobalIRQ();
}
void exitCriticalSection(void) {
	EnableGlobalIRQ(primask);
}
