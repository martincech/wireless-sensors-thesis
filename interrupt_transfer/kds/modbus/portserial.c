/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portserial.c,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */

#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- static functions ---------------------------------*/

#define ECHO_BUFFER_LENGTH 8

UART_Type *uart_type = UART0;
uart_handle_t g_uartHandle;
uart_transfer_t receiveXfer;
uint8_t g_rxBuffer[ECHO_BUFFER_LENGTH] = { 0 };

char c;


/* UART user callback */
void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status,
		void *userData);

/* ----------------------- Start implementation -----------------------------*/
void vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable) {
	/* If xRXEnable enable serial receive interrupts. If xTxENable enable
	 * transmitter empty interrupts.
	 */

	// do not switch before transmission is complete
	while (! (UART_GetStatusFlags(uart_type) & kUART_TransmissionCompleteFlag)) {
		;
	}

	if (xTxEnable) {
		UART_EnableTx(uart_type, true);
		UART_ClearStatusFlags(uart_type, kUART_TxDataRegEmptyFlag);
		UART_EnableInterrupts(uart_type, kUART_TxDataRegEmptyInterruptEnable);

	} else {
		UART_DisableInterrupts(uart_type, kUART_TxDataRegEmptyInterruptEnable);
		UART_ClearStatusFlags(uart_type, kUART_TxDataRegEmptyFlag);
		UART_EnableTx(uart_type, false);
	}

	if (xRxEnable) {
		UART_EnableRx(uart_type, true);
		UART_ClearStatusFlags(uart_type,
				kUART_RxDataRegFullFlag | kUART_RxOverrunFlag);
		UART_EnableInterrupts(uart_type,
				kUART_RxDataRegFullInterruptEnable
						| kUART_RxOverrunInterruptEnable);

	} else {
		UART_DisableInterrupts(uart_type,
				kUART_RxDataRegFullInterruptEnable
						| kUART_RxOverrunInterruptEnable);
		UART_ClearStatusFlags(uart_type,
				kUART_RxDataRegFullFlag | kUART_RxOverrunFlag);
		UART_EnableRx(uart_type, false);
	}


}

BOOL xMBPortSerialInit(UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits,
		eMBParity eParity) {
	uart_config_t config;
	uart_type = UART0;

	/*
	 * config.baudRate_Bps = 115200U;
	 * config.parityMode = kUART_ParityDisabled;
	 * config.stopBitCount = kUART_OneStopBit;
	 * config.txFifoWatermark = 0;
	 * config.rxFifoWatermark = 1;
	 * config.enableTx = false;
	 * config.enableRx = false;
	 */
	UART_GetDefaultConfig(&config);

	config.baudRate_Bps = ulBaudRate;
	if (eParity == MB_PAR_ODD) {
		config.parityMode = kUART_ParityOdd;
	} else if (eParity == MB_PAR_EVEN) {
		config.parityMode = kUART_ParityEven;
	}
	config.enableTx = true;
	config.enableRx = true;
	if (ucPORT == 1) {
		uart_type = UART1;
	}
	UART_Init(uart_type, &config, CLOCK_GetFreq(UART_CLKSRC));
	uart_type->MODEM |= UART_MODEM_TXRTSE(1); // set RTS when transmitting
	uart_type->MODEM |= UART_MODEM_TXRTSPOL(1); // RTS is set to 1 when transmitting



	if (uart_type == UART0) {
		EnableIRQ(UART0_RX_TX_IRQn);
	} else if (uart_type == UART1) {
		EnableIRQ(UART1_RX_TX_IRQn);
	}

	return TRUE;
}

BOOL xMBPortSerialPutByte(CHAR ucByte) {
	/* Put a byte in the UARTs transmit buffer. This function is called
	 * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
	 * called. */
	while (!(kUART_TxDataRegEmptyFlag & UART_GetStatusFlags(uart_type))) {
	}
	UART_WriteByte(uart_type, ucByte);
	return TRUE;
}

BOOL xMBPortSerialGetByte(CHAR * pucByte) {
	/* Return the byte in the UARTs receive buffer. This function is called
	 * by the protocol stack after pxMBFrameCBByteReceived( ) has been called.
	 */
	*pucByte = UART_ReadByte(uart_type);
	return TRUE;
}

/* Create an interrupt handler for the transmit buffer empty interrupt
 * (or an equivalent) for your target processor. This function should then
 * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 * a new character can be sent. The protocol stack will then call 
 * xMBPortSerialPutByte( ) to send the character.
 */
/* Create an interrupt handler for the receive interrupt for your target
 * processor. This function should then call pxMBFrameCBByteReceived( ). The
 * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 * character.
 */
void UART_IRQ_HANDLER() {
	uint32_t status = UART_GetStatusFlags(uart_type);
	uint32_t enabledInterrupts = UART_GetEnabledInterrupts(uart_type);
	if (((kUART_RxDataRegFullFlag | kUART_RxOverrunFlag) & status)
		&& (enabledInterrupts & (kUART_RxDataRegFullInterruptEnable | kUART_RxOverrunInterruptEnable))){
		//LED_TOGGLE();
		pxMBFrameCBByteReceived();
	}
	if ((kUART_TxDataRegEmptyFlag & status)
			&& (enabledInterrupts & kUART_TxDataRegEmptyInterruptEnable)) {
		pxMBFrameCBTransmitterEmpty();
	}
}

/* UART user callback */
void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status,
		void *userData) {
	userData = userData;
	LED_TOGGLE();

}
