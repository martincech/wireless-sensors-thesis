/*
 * ee071.h
 *
 *  Created on: 6 Nov 2016
 *      Author: D�vid Danaj
 */

#ifndef MODBUS_EE071_H_
#define MODBUS_EE071_H_
#include "port.h"

#define EE071_SLAVE_ID 247
#define EE071_TEMPERATURE_ADDR 0x27
#define EE071_REL_HUMIDITY_ADDR 0x29

SHORT readTemperature(void);
SHORT readRelHumidity(void);
BOOL readTempAndHumidity(SHORT *temp, SHORT *hum);

SHORT readRegister(USHORT addr);


#endif /* MODBUS_EE071_H_ */
