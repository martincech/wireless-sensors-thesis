# Project Progress Overview:

Initiation | Planning | ***Execution (calculations)*** | Execution (programming) | Optimization | Conclusion

## Milestones:

~~***25.4.*** - Initiation phase completed~~

~~***6.5.*** - Planning phase completed~~

~~***24.6.*** - PV191 - Project completed~~

***27.6.*** - Execution (calculations) completed

***26.8.*** - Execution (programming) completed

***21.10.*** - Optimization completed

***25.11.*** - Conclusion completed - Project code delivery (coding finished)

***20.12.*** - Thesis delivery


## Plan: [plan-wiki](https://github.com/ddanaj/Wireless-sensors---Thesis/wiki/Project-Plan)
Power consumption calculations: [Google Spreadsheet link](https://docs.google.com/spreadsheets/d/1Pc4gJPj8u6nvbXPOEZru_Njo7XY5ZzenLxF3G-qd6JI/edit?usp=sharing)

