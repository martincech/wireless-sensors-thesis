import csv
from collections import OrderedDict

class Logs:
    def __init__(self, file):
        self.data = self.readCSV(file)

    def readCSV(self, file):
        list = []
        with open(file) as csvfile:
            csvfile.next()
            reader = csv.DictReader(csvfile)
            for row in reader:
                list.append(row)
        return list




