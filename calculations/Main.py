import argparse
from Logs import Logs
from Configuration import Configuration
import string
import itertools
import csv


def outputCSV(file, data):
    """
    write all data to CSV file
    :param file: file to write to
    :param data: data that should be written to a file
    """
    with open(file, 'wb') as csvwrite:
        writer = csv.DictWriter(csvwrite, fieldnames=data[0].keys())
        writer.writeheader()
        writer.writerows(data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-mcu', required=True, action='store')
    parser.add_argument('-temp', required=True, action='store')
    parser.add_argument('-tenso', required=True, action='store')
    parser.add_argument('-co', required=True, action='store')
    parser.add_argument('-hum', required=True, action='store')
    parser.add_argument('-out', required=True, action='store')

    args = parser.parse_args()
    mcu = Logs(args.mcu)
    temp = Logs(args.temp)
    tenso = Logs(args.tenso)
    co = Logs(args.co)
    hum = Logs(args.hum)
    burst = ['yes', 'no']

    # This code might be used later, when we get relevant information that is missing in datasheets (timings, consumptions, ...)
    #
    # period = [1, 10, 30, 60, 600]
    # # all combinations of temp sensors and periods
    # tempPeriod = itertools.product(temp.data, period)
    # # all combinations of tenso sensors and periods
    # tensoPeriod = itertools.product(tenso.data, period)
    # # all combinations of CO2 sensors and periods
    # coPeriod = itertools.product(co.data, period)
    # # all combinations of humidity sensors and periods
    # humPeriod = itertools.product(hum.data, period)
    # # all combinations of sensors and their periods (1 sensor of each type)
    # sensorCombinations = itertools.product(tempPeriod, tensoPeriod, coPeriod, humPeriod)
    #
    # # all combinations of: mcu + 4x sensor (with all possible periods)
    # product = itertools.product(mcu.data, sensorCombinations)

    # all combinations of: mcu + 4x sensor of each type with all possible periods
    product = itertools.product(mcu.data, temp.data, tenso.data, co.data, hum.data, burst)

    # used to get total number of combinations
    i = 0
    configurations = []
    outputs = []
    # for each configuration
    for item in product:
        i += 1
        # create new Configuration object
        config = Configuration(item)
        # calculate energy consumptions
        config.calculate()
        # save configurations
        configurations.append(config)
        # save results of this configuration
        outputs.append(config.getOutput())

    # output results to CSV file
    outputCSV(args.out, outputs)



