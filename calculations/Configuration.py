import csv
from collections import OrderedDict

from math import ceil


class Configuration:
    # constant - how long does it take to measure data from sensor in seconds
    # should be read from CSV files, but value is not stated in datasheets
    MEASUREMENT_TIME = 0.2
    # constant - how much memory space is needed to store one data read form sensor
    # 4 bytes data + 6 bytes time
    DATA_SIZE = 10 # bytes

    def __init__(self, data):
        self.data = data
        # create lists with names of keys
        configNames = ['MCU', 'Sen_1', 'PER_1', 'Sen_2', 'PER_2', 'Sen_3', 'PER_3', 'Sen_4', 'PER_4']
        timePercentNames = ['MCU_t', 'Radio_t', 'S1_t', 'S2_t', 'S3_t', 'S4_t']
        energyPercentNames = ['MCU_ep', 'Radio_ep', 'S1_ep', 'S2_ep', 'S3_ep', 'S4_ep']
        energyRawNames = ['MCU_en', 'Radio_en', 'S1_en', 'S2_en', 'S3_en', 'S4_en']

        # create ordered dictioneries for storing results
        self.config = OrderedDict.fromkeys(configNames)
        self.timePercent = OrderedDict.fromkeys(timePercentNames)
        self.energyPercent = OrderedDict.fromkeys(energyPercentNames)
        self.energyRaw = OrderedDict.fromkeys(energyRawNames)
        # save information about configuration
        self.mcu = data[0]
        self.temp = data[1]
        self.tenso = data[2]
        self.co = data[3]
        self.hum = data[4]
        self.consumption = 0
        self.burst = data[5]

        self.updateConfig()



    def updateConfig(self):
        """
        update fields in config for correct output.
        """
        self.config['MCU'] = self.mcu['Name']
        self.config['Sen_1'] = self.temp['Name']
        self.config['PER_1'] = float(self.temp['Period'])
        self.config['Sen_2'] = self.tenso['Name']
        self.config['PER_2'] = float(self.tenso['Period'])
        self.config['Sen_3'] = self.co['Name']
        self.config['PER_3'] = float(self.co['Period'])
        self.config['Sen_4'] = self.hum['Name']
        self.config['PER_4'] = float(self.hum['Period'])

    def gcd(self, a, b):
        """Return greatest common divisor using Euclid's Algorithm."""
        while b:
            a, b = b, a % b
        return a

    def lcm(self, a, b):
        """Return lowest common multiple."""
        # if period is lower than 1 sec, round it up to get relevant results
        if a < 1:
            a = 1
        if b < 1:
            b = 1
        return a * b // self.gcd(a, b)

    def lcmm(self, *args):
        """Return lcm of args."""
        return reduce(self.lcm, args)

    def calculateMcuEnergyRaw(self):
        """
        returns average energy used by MCU during measurement interval.
        each reading of data from sensor is separated (there is no option to read from 2 sensors simultaneously for now)
        """
        # get lowest common multiple of periods - total time to calculate from
        self.totalTime = self.lcmm(self.config['PER_1'], self.config['PER_2'], self.config['PER_3'], self.config['PER_4'])
        # get number of calculations of each sensor in total time of calculations
        self.calculations1 = self.totalTime / self.config['PER_1']
        self.calculations2 = self.totalTime / self.config['PER_2']
        self.calculations3 = self.totalTime / self.config['PER_3']
        self.calculations4 = self.totalTime / self.config['PER_4']
        self.totalCalculations = sum([self.calculations1, self.calculations2, self.calculations3, self.calculations4])
        # time when MCU is running
        timeRun = self.MEASUREMENT_TIME * self.totalCalculations
        # timeRun can't be higher than total time of measurement
        if timeRun > self.totalTime:
            timeRun = self.totalTime
        timeSleep = self.totalTime - timeRun
        energy = (float(self.mcu['Sleep']) * timeSleep + float(self.mcu['Run']) * timeRun) / self.totalTime
        return energy

    def calculateRadioEnergyRaw(self):
        """
        returns average energy used by Radio during measurement interval.
        """
        energyRadio = 0
        headerSize = float(self.mcu['packet size']) - float(self.mcu['payload'])
        # convert Tx speed in kb/s to B/s
        txSpeed = float(self.mcu['Tx speed']) * 1024 / 8

        if self.burst is 'no':
            # without burst - send 1 packet for each measurement
            # timeSending = size of packet / sneding speed * number of packets
            timeSending = (self.DATA_SIZE + headerSize) / txSpeed * self.totalCalculations

        else:
            # with burst - fill each packet to maximum and send lowest possible number of packets
            # packets = number of measurements * size of measured data / how much can we fit into 1 packet - rounded up
            packets = ceil(self.totalCalculations * self.DATA_SIZE / float(self.mcu['payload']))
            # timeSending = size of packet * number of packets / sending speed
            timeSending = float(self.mcu['packet size']) * packets / txSpeed

        # Tx consumption * time to send one packet * number of packets to send
        energyTransmitting = float(self.mcu['Tx']) * timeSending
        timeSleep = self.totalTime - timeSending
        energyRadio = (float(self.mcu['Radio - Idle']) * timeSleep + energyTransmitting) / self.totalTime
        return energyRadio

    def calculateSensorEnergyRaw(self, data):
        """
        returns average energy used by sensor.
        for now, there is no calculations, because we don't have enough data from datasheets
        """
        return float(data['Consumption'])

    def calculateEnergyPercent(self):
        """
        calculate percentage of energy used by each part of configuration.
        """
        self.energyPercent['MCU_ep'] = self.energyRaw['MCU_en'] / self.consumption * 100
        self.energyPercent['Radio_ep'] = self.energyRaw['Radio_en'] / self.consumption*  100
        self.energyPercent['S1_ep'] = self.energyRaw['S1_en'] / self.consumption * 100
        self.energyPercent['S2_ep'] = self.energyRaw['S2_en'] / self.consumption * 100
        self.energyPercent['S3_ep'] = self.energyRaw['S3_en'] / self.consumption * 100
        self.energyPercent['S4_ep'] = self.energyRaw['S4_en'] / self.consumption * 100


    def calculateEnergyConsumption(self):
        """
        calculate average energy consumption used by each part of configuration.
        """
        self.energyRaw['MCU_en'] = self.calculateMcuEnergyRaw()
        self.energyRaw['Radio_en'] = self.calculateRadioEnergyRaw()
        self.energyRaw['S1_en'] = self.calculateSensorEnergyRaw(self.temp)
        self.energyRaw['S2_en'] = self.calculateSensorEnergyRaw(self.tenso)
        self.energyRaw['S3_en'] = self.calculateSensorEnergyRaw(self.co)
        self.energyRaw['S4_en'] = self.calculateSensorEnergyRaw(self.hum)
        self.consumption = sum(self.energyRaw.values())


    def calculate(self):
        self.calculateEnergyConsumption()
        self.calculateEnergyPercent()


    def getOutput(self):
        output = self.config
        output.update(self.timePercent)
        output.update(self.energyPercent)
        output.update(self.energyRaw)
        return output

