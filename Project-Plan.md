# Project Plan


***25.4. - Thesis description completed (with outputs and success criteria) - Initiation phase completed***

***6.5. - Risks, Reporting setup, excel layout for calculations - Planning phase completed***

27.5. - Calculations with basic setup (Digi Xbee + integrated MCU)

3.6. - Calculations extended for other MCUs / radios / sensors - depending on results from basic setup

10.6. - MCU connection scheme

17.6. - Application protocol

***24.6. - PV191 - Project completed***

***27.6. - Execution (calculations) completed***

8.7. - Read data from Temp., CO2, Humidity

14.7 - Read weight data from adc/tensiometer

12.8. - MCU algorithm

***26.8. - Execution (programming) completed***

To-Do: plan optimization phase

***21.10. - Optimization completed***

To-Do: plan conclusion phase

***25.11. - Conclusion completed - Project code delivery (coding finished)***

***20.12. - Thesis delivery***
